import React, { Component } from 'react';
import './ImageGrid.css';

import Image from '../Image/Image';

class ImageGrid extends Component {
  // constructor(props) {
  //   super(props);
  // }

  render() {
    const images = this.props.images;
    let html;

    if ( this.props.images ) {
      html = (
        images.map( (image, index) => (
          <Image source={image} key={ index } />
        ))
      );
    } else {
      html = (
        <p>Sorry no images!</p>
      )
    }
    return (
      <div className="image-grid">
        { html }
      </div>
    );
  }
}

export default ImageGrid;